# Step Project 2

## Section made by Olya:
- header
- section-intro
- section-bio
- section-logos
- section-selected
- section-team

## Section made by Timur:
- what-i-do
- clients
- contact
- footer

## How to build project:
- `npm run build`

## How to run project:
- `npm run dev`
